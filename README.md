# zellij demo

An OCI container with zellij pre-installed, along with a TUI-based presentation
that introduces you to the basic features of zellij while using it.


## What is this?

[zellij][zellij-web] (available on [GitHub][zellij-git]) is a modern terminal
multiplexer and terminal workspace. This repository is meant to help users
unfamiliar with the substance to get a feeling for what zellij is capable of
and how it may improve/support their terminal-based workflows.

All the relevant content is inside `zellij.md`, which is a plain markdown file.
If you want to digest the contents of this file in a more interactive fashion,
follow the instructions below to run this container.

The following software is available in this container:
- [zellij][zellij-web]: The default entry-point in the container
- [lookatme][lookatme]: A terminal-based markdown presenter


## How to use it?

To run the container you must have `podman` or `docker` installed. Once that's
done, run the container with (substitute `podman` with `docker` if needed):

```bash
$ podman run --rm -it --detach-keys="" \
    registry.gitlab.com/hartang/zellij/demo:latest
```

Once inside the container you will be presented with an empty zellij session.
If you want to see the interactive presentation and follow along, type (inside
the container):

```bash
$ lookatme /root/zellij.md
```


## Additional information

The presentation inside `zellij.md` was first held at the weekly ''Chaostreff''
of the [NoName e.V.][noname-ev]. The recording is available online at (TODO).


[zellij-web]: https://zellij.dev/
[zellij-git]: https://github.com/zellij-org/zellij
[lookatme]: https://github.com/d0c-s4vage/lookatme
[noname-ev]: https://www.noname-ev.de/
