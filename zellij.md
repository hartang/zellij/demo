---
title: zellij - a modern terminal multiplexer
author: Andreas Hartmann (@hartan)
date: 2023.03.09
extensions: []
---

# Before we begin

Navigating this presentation:
- `l`/`j`/`SPACE`: Next slide
- `h`/`k`/`BACKSPACE`: Previous slide
- `q`: Quit presentation

To read a hyperlink (by default: underlined and colored blue), click it once to
reveal the URL. Click it again to hide the URL.


---
```text
                                                   _jw, |Mw_                     
                                               _jwMMMMl |MMMMMw_                 
                                              0MMMMMMM   #MMMMMMMMw_             
                                       _jw0n  MMMM^~       ~*0MMMMMMMww          
                                   _jwMMMMMM  "~   _wwMM0my_  "~MMMM@~~ _wy_     
                               ,wwMMMMMMM@^'   _wwMMMMMMMMMM0m_,     mMMMMMM0my, 
                              pMMMMMMM^~   _wwMMMMMMMMMMMMMMMMMMMm_,   ~M0MMMMMMA
                              #MMMMP   _wwMMMMMMMMMMMMMMMMMMMMMMMMMMMw_   ~#MMMM'
                              #MMMM'  gMMMMMM0MMMMMMMMMMMMMMMMMMMMMMMMMMy   ~~ __
                              #MMMM'  MMMMMM~   ~0MMMMMMMMMMMMMMMMMMMMMM6   jMMMM
                              #MMMM'  MMMMMMm,    "MMMMMMMMMMMMMMMMMMMMM6  4MMMM0
                              #MMMM'  MMMMMMMMM_    ~MMMMMMMMMMMMMMMMMMM6  !MMM~'
                              #MMMM'  MMMMMMMMMMM     _MMMMMMMMMMMMMMMMM6   ,wwmy
                              #MMMM'  MMMMMMMMM'    _MMMMMMMMMMMMMMMMMMM6  qMMMM#
                              #MMMM'  MMMMM0~     w0MMMMMMMMMMMMMMMMMMMMF   MMMM#
                              #MMMM'  MMMMMMw, _wMMMMMM            MMMMMf   MMMM#
                              #MMMM'  MMMMMMMMMMMMMMMMMwwwwwmwwmmwwMMMMMf    ~0M#
                              0MV^'    ~~MMMMMMMMMMMMMMMMMMMMMMMMMMMM@~"  _dm_ ~#
                                __wMMww_   ~~MMMMMMMMMMMMMMMMMMMM@~~   _wMMMMMm, 
                                ~*0MMMMMMz     "~MMMMMMMMMMMM@~~   _wMMMMMMMMM~' 
                                    ~*0MM0  Mww_   ~MMMM0@~"    _wMMMMMMMM~~     
                                        ~^  MMMMMww_       _.  jMMMMMM~~         
                                            "*0MMMMMMwmwww0~ ,#MMM^~             
                                               "~MMMMMMMMP  wM^~                 
                                                   ~~MM0~  ~                     
```

---
# General things

## Disclaimer

> **I'm a zellij Co-maintainer**
>
>   I may be biased, sorry for that ;-)
>
> **This is beta software**

<!-- stop -->

## Agenda for today

- What is zellij and where does it come from?
- Quick application walkthrough
- Introduction to layout system


---
# A few more things

- All content provided here is available on [GitLab](https://gitlab.com/hartang/zellij/demo)
    - The presentation is written in Markdown
    - The software is packaged into a container
- I'll show you a vanilla zellij setup
    - But there's quite a lot you can customize about it!
- How to read keybindings: 

```
  press together ---+      +---- brackets (optional)
                    |      |
                Alt + <hjkl>
                 |      |
  modifier ------+      +----- group of keys
```


---
# About zellij

- Started out in 2021
- Lead developer & project initiator: Aram Drevekenin
- Written in Rust
- The goal: Become a ''terminal workspace''
- Originally named *mosaic*, now *zellij*
- Developed on [GitHub](https://github.com/zellij-org/zellij)
- Documentation & more on the [Website](https://zellij.dev/)

<!-- stop -->

> [zellij](https://en.wikipedia.org/wiki/Zellij) is actually a style of mosaic very common in Morocco

---
# Features

- terminal multiplexer (*you may have guessed by now*)
- uses panes, which are organized in tabs
- panes can be resized, moved, or made to float
- sophisticated layout system
- plugin system with WASM-based plugins
- rich UI out of the box, displays hints at the bottom

<!-- stop -->

> No more scrolling through a long list of keybindings!


---
# The basics

- At the top is the tab-bar
    - Highlights the current tab
    - Reacts to mouse-events

<!-- stop -->

- At the bottom is the status-bar
    - Highlights the current input mode (first line)
    - Shows tips/mode-specific keybindings (second line)

<!-- stop -->

- Create new panes with `Alt + n`
- Move between panes with `Alt + <hjkl>`
- In-/Decrease pane size with `Alt + <+->`


---
# Locked mode

- Accessible via `Ctrl + g`
- Does ... nothing, really
- Very handy, e.g. when
    - zellij keybindings collide with focused application
    - nesting zellij sessions

---
# Tiled Panes

> The keybindings below are available inside **PANE** mode: `Ctrl + p`
>
> All actions refer to the currently focused (highlighted) pane

| Action | Keybinding |
| -----: | :--------- |
| Move focus | `<hjkl>`/`<←↓↑→>` |
| Create pane (smart) | `n` |
| Close pane | `x` |
| Change pane name | `c` |
| Create pane (below) | `d` |
| Create pane (right) | `r` |
| Make pane fullscreen | `f` |
| Toggle pane frames | `z` |
| Focus next pane | `p` |
| Leave **PANE** mode | `ENTER`/`ESC` |

---
# Tiled Panes - Bonus

Unnamed panes understand ANSI sequences to rename terminals:

```bash
$ echo -e "\e]0;Hello Terminal\a"
```

---
# Floating panes

> The keybindings below are available inside **PANE** mode: `Ctrl + p`

Floating panes are like a separate mode to interact with a second set of panes
in the same tab.

| Action | Keybinding |
| -----: | :--------- |
| Create floating pane | `n`/`Alt + n` |
| Toggle floating panes | `w` |
| Embed/float pane | `e` |

<!-- stop -->

## Bonus

- Floating panes can overlap
- Floating panes can be moved with the mouse
- Clicking next to a floating pane goes to tiled mode

---
# Tabs

> The keybindings below are available inside **TAB** mode: `Ctrl + t`

| Action | Keybinding |
| -----: | :--------- |
| Move focus to other tabs | `<hjkl>`/`<←↓↑→>` |
| Create tab | `n` |
| Close tab | `x` |
| Rename tab | `r` |
| Sync input of all panes in tab | `s` |
| Toggle current/most-recent tab | `TAB` |
| Leave **TAB** mode | `ENTER`/`ESC` |

---
# Resizing panes

> The keybindings below are available inside **RESIZE** mode: `Ctrl + n`

| Action | Keybinding |
| -----: | :--------- |
| Increase size in direction | `<hjkl>`/`<←↓↑→>` |
| Decrease size from direction | `<HJKL>` |
| Generic in-/decrease size | `<+->` |
| Leave **RESIZE** mode | `ENTER`/`Ctrl + n`/`ESC` |

---
# Resizing panes -  Bonus

- Resizing detects corners/aligned panes and keeps them intact

```
  +-----+-----+         +----+------+
  |     |     |         | -- |  +-  |
  |     |     |         +----+------+
  +-----+-----+   ==>   |    |      |
  |     |     |         | +- |  ++  |
  |     |     |         |    |      |
  +-----+-----+         +----+------+
```

- An increasing resize towards a screen boundary reduces the panes size instead

---
# Moving panes

> The keybindings below are available inside **MOVE** mode: `Ctrl + h`

| Action | Keybinding |
| -----: | :--------- |
| Switch location with pane at | `<hjkl>`/`<←↓↑→>` |
| Switch with next pane | `n` |
| Switch with previous pane | `p` |
| Leave **MOVE** mode | `ENTER`/`ESC` |

---
# Searching panes

> The keybindings below are available inside **SEARCH** mode: `Ctrl + s`

| Action | Keybinding |
| -----: | :--------- |
| Scroll single line down/up | `<jk>`/`<↓↑>` |
| Scroll half-page down/up | `<du>` |
| Scroll full-page down/up | `<lh>`/`<→←>`/`<PgDn\|PgUp>` |
| Edit scrollback in `$EDITOR` | `e` |
| Enter search term | `s` |
| Leave **SEARCH** mode | `ENTER`/`ESC` |

---
# Searching panes -  Bonus

Additional keybindings after a search-term has been entered

| Action | Keybinding |
| -----: | :--------- |
| Search down | `n` |
| Search up | `p` |
| Toggle case sensitivity | `c` |
| Toggle wrap search at bottom/top | `w` |
| Match whole words only | `o` |

---
# Sessions

> The keybindings below are available inside **SESSION** mode: `Ctrl + o`

| Action | Keybinding |
| -----: | :--------- |
| Detach | `d` |
| Leave **SESSION** mode | `ENTER`/`ESC` |

<!-- stop -->

## Bonus

- Sessions will persist until quit/killed or the system reboots
- Sessions can be named (*but only when creating them*)
- List sessions with `zellij ls`
- Reattach to sessions with `zellij attach`


---
# Quit

You can figure that one out for yourself. ;-)

---
# Command panes - `zellij run`

- Special type of pane specialized on running commands
- Configured either via layouts (see later), or `zellij run`
- Try it out yourself:

```bash
$ zellij run -f -s -n "LIST ETC" -- ls -lR /etc
```

- See all options with `zellij run --help`

---
# Scripting zellij - `zellij action`

- Interaction with zellij primarily takes place via keybindings
    - *But*: There are so few keys for so many cool things to do...
    - ...and many applications need their own set of keys for interaction
- Besides: In my opinion scripting keyboard-input is weird

<!-- stop -->

- **Solution**: `zellij action`
    - Makes all actions that can be bound to keys accessible via CLI
    - For example: open a new tab with a given layout:

```bash
$ zellij action new-tab -l split -n "ACTIONS ROCK"
```

---
# Extending zellij with plugins

- Plugins can be written in any langauge that compiles to WASM
- User plugins (that we know of) exist in Rust and Zig
- Comes with four plugins bundled in the application:
    - status-bar
    - tab-bar
    - compact-bar
    - strider

<!-- stop -->

- There is an API for plugins
    - We're always looking for cool ideas to help us shape this API
- Plugin system will likely undergo **major rewrite** for next release

---
# Configuration and the KDL language

- Configuration is written in [KDL](https://kdl.dev/)
    - Goal: Looks/reads like a list of CLI commands
- All configuration lives in `~/.config/zellij/`
    - Configuration for zellij (application settings + keybindings)
    - Layouts + Swap layouts (see next slide)
    - *Themes are planned, check back later*

<!-- stop -->

Default config excerpt:
```kdl
keybinds {
    locked {
        bind "Ctrl g" { SwitchToMode "Normal"; }
    }
    ...
    resize {
        bind "Ctrl n" { SwitchToMode "Normal"; }
        bind "h" "Left" { Resize "Increase Left"; }
        bind "j" "Down" { Resize "Increase Down"; }
    }
    ...
}
```

---
# Layouts and how to write them

The default layout

```kdl
layout {
    pane size=1 borderless=true {
        plugin location="zellij:tab-bar"
    }
    pane
    pane size=2 borderless=true {
        plugin location="zellij:status-bar"
    }
}
```

> Available as layout named `default`

---
# Layouts and how to write them - Part 2

Defining command panes

```kdl
layout {
    pane {
        command "ls"
        args "-l" "-R" "/etc"
        name "LIST FILES"
        start_suspended true
    }
}
```

> Available as layout named `ex1`

---
# Layouts and how to write them - Part 3

Pane templates

```kdl
layout {
    pane_template name="editor" {
        focus true
        command "bash"
        args "-ic" "nvim"
        name "EDITOR"
    }

    pane split_direction="vertical" {
        editor name="A"
        editor name="B"
    }
}

```

> Available as layout named `ex2`

---
# Swap layouts

- New feature in version 0.35.1 and up
- Idea: Rearrange visible panes ''intelligently''
    - Provide a set of (user-configurable) ''swap-layout'' specifications
    - Access these swap layouts via keybindings: `Alt + <[]>`

<!-- stop -->

- Defined in special `swap_tiled_layout` root nodes in the KDL config

```
tab_template name="ui" {
   children
}

swap_tiled_layout name="vertical" {
    ui max_panes=5 {
        pane split_direction="vertical" {
            pane
            pane { children; }
        }
    }
    ...
}
```

---
# Multiplayer mode

- Why play solo? ;-)
- Try connecting to a running session from another terminal...

```bash
$ docker exec -it zellij-demo -- zellij attach zellij-demo
```


---
# The End

This was just a quick walkthrough, there's more to discover!

Additional resources:
- [zellij Website (+ documentation)](https://zellij.dev/)
- [zellij on GitHub](https://github.com/zellij-org/zellij)
