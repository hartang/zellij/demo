FROM registry.fedoraproject.org/fedora-minimal:37

# Install required dependencies
RUN microdnf install -y python3-pip tar curl gzip unzip neovim && \
    pip3 install lookatme && \
    rm -rf /var/cache/yum || \
    { echo "failed to install required system packages"; exit 1; }

# Install zellij
RUN curl -LO https://github.com/zellij-org/zellij/releases/download/v0.35.2/zellij-x86_64-unknown-linux-musl.tar.gz && \
    tar -xf zellij-x86_64-unknown-linux-musl.tar.gz && mv zellij /usr/local/bin/zellij && \
    rm -rf zellij-x86_64-unknown-linux-musl.tar.gz || \
    { echo "failed to download zellji bin"; exit 1; }

# Install party-parrot
RUN curl -LO https://github.com/jmhobbs/terminal-parrot/releases/download/1.1.1/terminal-parrot-linux-amd64.zip && \
    unzip terminal-parrot-linux-amd64.zip && mv terminal-parrot /usr/local/bin/parrot && \
    rm -rf terminal-parrot-linux-amd64.zip LICENSE README.md || \
    { echo "failed to download parrot bin"; exit 1; }

WORKDIR /root
COPY config/zellij /root/.config/zellij
COPY zellij.md /root/zellij.md
COPY assets /root/assets

ENTRYPOINT [ "/usr/local/bin/zellij" ]
CMD [ "-s", "live-demo" ]
