#!/usr/bin/env bash

DIR="$(dirname "$0")"

podman run --rm -it --detach-keys="" \
    --name "zellij-demo" --replace \
    -v "$DIR/zellij:/root/.config/zellij:z" \
    -v "$DIR/assets:/root/assets:z" \
    -v "$DIR/assets/zellij:/usr/local/bin/zellij:z" \
    -v "$DIR/zellij.md:/root/zellij.md:z" \
    -w "/root" registry.gitlab.com/hartang/zellij/demo:0 \
    -s "zellij-demo" -l "slides"
